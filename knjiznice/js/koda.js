
var baseUrl = 'https://rest.ehrscape.com/rest/v1';
var queryUrl = baseUrl + '/query';

var username = "ois.seminar";
var password = "ois4fri";

var ehrid="";


/**
 * Prijava v sistem z privzetim uporabnikom za predmet OIS in pridobitev
 * enolične ID številke za dostop do funkcionalnosti
 * @return enolični identifikator seje za dostop do funkcionalnosti
 */
function getSessionId() {
    var response = $.ajax({
        type: "POST",
        url: baseUrl + "/session?username=" + encodeURIComponent(username) +
        "&password=" + encodeURIComponent(password),
        async: false
    });
    return response.responseJSON.sessionId;
}


/**
 * Kreiraj nov EHR zapis za pacienta in dodaj osnovne demografske podatke.
 * V primeru uspešne akcije izpiši sporočilo s pridobljenim EHR ID, sicer
 * izpiši napako.
 */
function kreirajEHRzaBolnika() {
    sessionId = getSessionId();

    var ime = $("#kreirajIme").val();
    var priimek = $("#kreirajPriimek").val();
    var datumRojstva = $("#kreirajDatumRojstva").val();

    if (!ime || !priimek || !datumRojstva || ime.trim().length == 0 ||
        priimek.trim().length == 0 || datumRojstva.trim().length == 0) {
        $("#kreirajSporocilo").html("<span class='obvestilo label " +
            "label-warning fade-in'>Prosim vnesite zahtevane podatke!</span>");
    } else {
        $.ajaxSetup({
            headers: {"Ehr-Session": sessionId}
        });
        $.ajax({
            url: baseUrl + "/ehr",
            type: 'POST',
            success: function (data) {
                var ehrId = data.ehrId;
				ehrid=ehrId;
                var partyData = {
                    firstNames: ime,
                    lastNames: priimek,
                    dateOfBirth: datumRojstva,
                    partyAdditionalInfo: [{key: "ehrId", value: ehrId}]
                };
                $.ajax({
                    url: baseUrl + "/demographics/party",
                    type: 'POST',
                    contentType: 'application/json',
                    data: JSON.stringify(partyData),
                    success: function (party) {
                        if (party.action == 'CREATE') {
                            $("#kreirajSporocilo").append("<span class='obvestilo " +
                                "label label-success fade-in'>Uspešno kreiran EHR '" +
                                ehrId + "'.</span><br/>");
                            $("#preberiEHRid").val(ehrId);
                        }
                    },
                    error: function(err) {
                        $("#kreirajSporocilo").html("<span class='obvestilo label " +
                            "label-danger fade-in'>Napaka '" +
                            JSON.parse(err.responseText).userMessage + "'!");
                    }
                });
            }
        });
    }
}


/**
 * Za podan EHR ID preberi demografske podrobnosti pacienta in izpiši sporočilo
 * s pridobljenimi podatki (ime, priimek in datum rojstva).
 */
function preberiEHRodBolnika() {
    sessionId = getSessionId();

    var ehrId = $("#preberiEHRid").val();

    if (!ehrId || ehrId.trim().length == 0) {
        $("#preberiSporocilo").html("<span class='obvestilo label label-warning " +
            "fade-in'>Prosim vnesite zahtevan podatek!");
    } else {
        $.ajax({
            url: baseUrl + "/demographics/ehr/" + ehrId + "/party",
            type: 'GET',
            headers: {"Ehr-Session": sessionId},
            success: function (data) {
                var party = data.party;
                $("#preberiSporocilo").html("<span class='obvestilo label " +
                    "label-success fade-in'>Bolnik '" + party.firstNames + " " +
                    party.lastNames + "', ki se je rodil '" + party.dateOfBirth +
                    "'.</span>");
            },
            error: function(err) {
                $("#preberiSporocilo").html("<span class='obvestilo label " +
                    "label-danger fade-in'>Napaka '" +
                    JSON.parse(err.responseText).userMessage + "'!");
            }
        });
    }
}


/**
 * Za dodajanje vitalnih znakov pacienta je pripravljena kompozicija, ki
 * vključuje množico meritev vitalnih znakov (EHR ID, datum in ura,
 * telesna višina, telesna teža, sistolični in diastolični krvni tlak,
 * nasičenost krvi s kisikom in merilec).
 */
function dodajMeritveVitalnihZnakov() {
    sessionId = getSessionId();

    var ehrId = $("#dodajVitalnoEHR").val();
    var datumInUra = $("#dodajVitalnoDatumInUra").val();
    var telesnaVisina = $("#dodajVitalnoTelesnaVisina").val();
    var telesnaTeza = $("#dodajVitalnoTelesnaTeza").val();
    var telesnaTemperatura = $("#dodajVitalnoTelesnaTemperatura").val();
    var sistolicniKrvniTlak = $("#dodajVitalnoKrvniTlakSistolicni").val();
    var diastolicniKrvniTlak = $("#dodajVitalnoKrvniTlakDiastolicni").val();
    var nasicenostKrviSKisikom = $("#dodajVitalnoNasicenostKrviSKisikom").val();
    var merilec = $("#dodajVitalnoMerilec").val();

    if (!ehrId || ehrId.trim().length == 0) {
        $("#dodajMeritveVitalnihZnakovSporocilo").html("<span class='obvestilo " +
            "label label-warning fade-in'>Prosim vnesite zahtevane podatke!</span>");
    } else {
        $.ajaxSetup({
            headers: {"Ehr-Session": sessionId}
        });
        var podatki = {
            // Struktura predloge je na voljo na naslednjem spletnem naslovu:
            // https://rest.ehrscape.com/rest/v1/template/Vital%20Signs/example
            "ctx/language": "en",
            "ctx/territory": "SI",
            "ctx/time": datumInUra,
            "vital_signs/height_length/any_event/body_height_length": telesnaVisina,
            "vital_signs/body_weight/any_event/body_weight": telesnaTeza,
            "vital_signs/body_temperature/any_event/temperature|magnitude": telesnaTemperatura,
            "vital_signs/body_temperature/any_event/temperature|unit": "°C",
            "vital_signs/blood_pressure/any_event/systolic": sistolicniKrvniTlak,
            "vital_signs/blood_pressure/any_event/diastolic": diastolicniKrvniTlak
        };
        var parametriZahteve = {
            ehrId: ehrId,
            templateId: 'Vital Signs',
            format: 'FLAT'
        };
        $.ajax({
            url: baseUrl + "/composition?" + $.param(parametriZahteve),
            type: 'POST',
            contentType: 'application/json',
            data: JSON.stringify(podatki),
            success: function (res) {
                $("#dodajMeritveVitalnihZnakovSporocilo").html(
                    "<span class='obvestilo label label-success fade-in'>" +
                    res.meta.href + ".</span>");
            },
            error: function(err) {
                $("#dodajMeritveVitalnihZnakovSporocilo").html(
                    "<span class='obvestilo label label-danger fade-in'>Napaka '" +
                    JSON.parse(err.responseText).userMessage + "'!");
            }
        });
    }
}


/**
 * Pridobivanje vseh zgodovinskih podatkov meritev izbranih vitalnih znakov
 * (telesna temperatura, filtriranje telesne temperature in telesna teža).
 * Filtriranje telesne temperature je izvedena z AQL poizvedbo, ki se uporablja
 * za napredno iskanje po zdravstvenih podatkih.
 */
function preberiMeritveVitalnihZnakov() {
    sessionId = getSessionId();

    var ehrId = $("#meritveVitalnihZnakovEHRid").val();
    var tip = $("#preberiTipZaVitalneZnake").val();

    if (!ehrId || ehrId.trim().length == 0 || !tip || tip.trim().length == 0) {
        $("#preberiMeritveVitalnihZnakovSporocilo").html("<span class='obvestilo " +
            "label label-warning fade-in'>Prosim vnesite zahtevan podatek!");
    } else {
        $.ajax({
            url: baseUrl + "/demographics/ehr/" + ehrId + "/party",
            type: 'GET',
            headers: {"Ehr-Session": sessionId},
            success: function (data) {
                var party = data.party;
                $("#rezultatMeritveVitalnihZnakov").html("<br/><span>Pridobivanje " +
                    "podatkov za <b>'" + tip + "'</b> bolnika <b>'" + party.firstNames +
                    " " + party.lastNames + "'</b>.</span><br/><br/>");
                if (tip == "telesna temperatura") {
                    $.ajax({
                        url: baseUrl + "/view/" + ehrId + "/" + "body_temperature",
                        type: 'GET',
                        headers: {"Ehr-Session": sessionId},
                        success: function (res) {
                            if (res.length > 0) {
                                var results = "<table class='table table-striped " +
                                    "table-hover'><tr><th>Datum in ura</th>" +
                                    "<th class='text-right'>Telesna temperatura</th></tr>";
								var a=[];
                                for (var i in res) {
                                    results += "<tr><td>" + res[i].time +
                                        "</td><td class='text-right'>" + res[i].temperature +
                                        " " + res[i].unit + "</td>";
									a.push({label:res[i].time,y:res[i].temperature});
                                }
                                results += "</table>";
								var chart = new CanvasJS.Chart("nar", {
								theme: "theme2",//theme1
								title:{
									text: "Diagram"
								},
								animationEnabled: false,   // change to true
								data: [
									{
										// Change type to "bar", "area", "spline", "pie",etc.
										type: "column",
										dataPoints: a
									}
								]
								});
								chart.render();
                                $("#rezultatMeritveVitalnihZnakov").append(results);
                            } else {
                                $("#preberiMeritveVitalnihZnakovSporocilo").html(
                                    "<span class='obvestilo label label-warning fade-in'>" +
                                    "Ni podatkov!</span>");
                            }
                        },
                        error: function() {
                            $("#preberiMeritveVitalnihZnakovSporocilo").html(
                                "<span class='obvestilo label label-danger fade-in'>Napaka '" +
                                JSON.parse(err.responseText).userMessage + "'!");
                        }
                    });
                } else if (tip == "telesna teža") {
                    $.ajax({
                        url: baseUrl + "/view/" + ehrId + "/" + "weight",
                        type: 'GET',
                        headers: {"Ehr-Session": sessionId},
                        success: function (res) {
                            if (res.length > 0) {
                                var results = "<table class='table table-striped " +
                                    "table-hover'><tr><th>Datum in ura</th>" +
                                    "<th class='text-right'>Telesna teža</th></tr>";
								var a=[];
                                for (var i in res) {
                                    results += "<tr><td>" + res[i].time +
                                        "</td><td class='text-right'>" + res[i].weight + " " 	+
                                        res[i].unit + "</td>";
										a.push({label:res[i].time,y:res[i].weight});
                                }
                                results += "</table>";
								var chart = new CanvasJS.Chart("nar", {
								theme: "theme2",//theme1
								title:{
									text: "Diagram"
								},
								animationEnabled: false,   // change to true
								data: [
									{
										// Change type to "bar", "area", "spline", "pie",etc.
										type: "column",
										dataPoints: a
									}
								]
								});
								chart.render();
                                $("#rezultatMeritveVitalnihZnakov").append(results);
                            } else {
                                $("#preberiMeritveVitalnihZnakovSporocilo").html(
                                    "<span class='obvestilo label label-warning fade-in'>" +
                                    "Ni podatkov!</span>");
                            }
                        },
                        error: function() {
                            $("#preberiMeritveVitalnihZnakovSporocilo").html(
                                "<span class='obvestilo label label-danger fade-in'>Napaka '" +
                                JSON.parse(err.responseText).userMessage + "'!");
                        }
                    });
                } else if (tip == "telesna temperatura AQL") {
                    var AQL =
                        "select " +
                        "t/data[at0002]/events[at0003]/time/value as cas, " +
                        "t/data[at0002]/events[at0003]/data[at0001]/items[at0004]/value/magnitude as temperatura_vrednost, " +
                        "t/data[at0002]/events[at0003]/data[at0001]/items[at0004]/value/units as temperatura_enota " +
                        "from EHR e[e/ehr_id/value='" + ehrId + "'] " +
                        "contains OBSERVATION t[openEHR-EHR-OBSERVATION.body_temperature.v1] " +
                        "where t/data[at0002]/events[at0003]/data[at0001]/items[at0004]/value/magnitude<35 " +
                        "order by t/data[at0002]/events[at0003]/time/value desc " +
                        "limit 10";
                    $.ajax({
                        url: baseUrl + "/query?" + $.param({"aql": AQL}),
                        type: 'GET',
                        headers: {"Ehr-Session": sessionId},
                        success: function (res) {
                            var results = "<table class='table table-striped table-hover'>" +
                                "<tr><th>Datum in ura</th><th class='text-right'>" +
                                "Telesna temperatura</th></tr>";
                            if (res) {
                                var rows = res.resultSet;
                                for (var i in rows) {
                                    results += "<tr><td>" + rows[i].cas +
                                        "</td><td class='text-right'>" +
                                        rows[i].temperatura_vrednost + " " 	+
                                        rows[i].temperatura_enota + "</td>";
                                }
                                results += "</table>";
                                $("#rezultatMeritveVitalnihZnakov").append(results);
                            } else {
                                $("#preberiMeritveVitalnihZnakovSporocilo").html(
                                    "<span class='obvestilo label label-warning fade-in'>" +
                                    "Ni podatkov!</span>");
                            }

                        },
                        error: function() {
                            $("#preberiMeritveVitalnihZnakovSporocilo").html(
                                "<span class='obvestilo label label-danger fade-in'>Napaka '" +
                                JSON.parse(err.responseText).userMessage + "'!");
                        }
                    });
                }
            },
            error: function(err) {
                $("#preberiMeritveVitalnihZnakovSporocilo").html(
                    "<span class='obvestilo label label-danger fade-in'>Napaka '" +
                    JSON.parse(err.responseText).userMessage + "'!");
            }
        });
    }
}

function generiraj() {
	document.getElementById("kreirajIme").value="Ime";
	document.getElementById("kreirajPriimek").value="Prezime";
	document.getElementById("kreirajDatumRojstva").value="1999-09-19";
	kreirajEHRzaBolnika();
	setTimeout(function() {
		document.getElementById("dodajVitalnoEHR").value=ehrid;
		document.getElementById("dodajVitalnoDatumInUra").value="2014-11-21T11:40Z";
		document.getElementById("dodajVitalnoTelesnaVisina").value="170";
		document.getElementById("dodajVitalnoTelesnaTeza").value="60";
		document.getElementById("dodajVitalnoTelesnaTemperatura").value="37";
		document.getElementById("dodajVitalnoKrvniTlakSistolicni").value="118";
		document.getElementById("dodajVitalnoKrvniTlakDiastolicni").value="92";
		dodajMeritveVitalnihZnakov();
		setTimeout(function() {
			document.getElementById("kreirajIme").value="Ime2";
			document.getElementById("kreirajPriimek").value="Prezime2";
			document.getElementById("kreirajDatumRojstva").value="1999-09-19";
			kreirajEHRzaBolnika();
			setTimeout(function() {
				document.getElementById("dodajVitalnoEHR").value=ehrid;
				document.getElementById("dodajVitalnoDatumInUra").value="2014-01-11T11:40Z";
				document.getElementById("dodajVitalnoTelesnaVisina").value="179";
				document.getElementById("dodajVitalnoTelesnaTeza").value="69";
				document.getElementById("dodajVitalnoTelesnaTemperatura").value="38";
				document.getElementById("dodajVitalnoKrvniTlakSistolicni").value="115";
				document.getElementById("dodajVitalnoKrvniTlakDiastolicni").value="79";
				dodajMeritveVitalnihZnakov();
				setTimeout(function() {
					document.getElementById("kreirajIme").value="Ime3";
					document.getElementById("kreirajPriimek").value="Prezime3";
					document.getElementById("kreirajDatumRojstva").value="1988-08-18";
					kreirajEHRzaBolnika();
					setTimeout(function() {
						document.getElementById("dodajVitalnoEHR").value=ehrid;
						document.getElementById("dodajVitalnoDatumInUra").value="2014-01-11T11:40Z";
						document.getElementById("dodajVitalnoTelesnaVisina").value="188";
						document.getElementById("dodajVitalnoTelesnaTeza").value="88";
						document.getElementById("dodajVitalnoTelesnaTemperatura").value="37.5";
						document.getElementById("dodajVitalnoKrvniTlakSistolicni").value="188";
						document.getElementById("dodajVitalnoKrvniTlakDiastolicni").value="99";
						dodajMeritveVitalnihZnakov();
					}, 50);
				}, 50);
			}, 50);
		}, 50);
	}, 50);
}


$(document).ready(function() {

    /**
     * Napolni testne vrednosti (ime, priimek in datum rojstva) pri kreiranju
     * EHR zapisa za novega bolnika, ko uporabnik izbere vrednost iz
     * padajočega menuja (npr. Pujsa Pepa).
     */
    $('#preberiPredlogoBolnika').change(function() {
        $("#kreirajSporocilo").html("");
        var podatki = $(this).val().split(",");
        $("#kreirajIme").val(podatki[0]);
        $("#kreirajPriimek").val(podatki[1]);
        $("#kreirajDatumRojstva").val(podatki[2]);
    });

    /**
     * Napolni testni EHR ID pri prebiranju EHR zapisa obstoječega bolnika,
     * ko uporabnik izbere vrednost iz padajočega menuja
     * (npr. Dejan Lavbič, Pujsa Pepa, Ata Smrk)
     */
    $('#preberiObstojeciEHR').change(function() {
        $("#preberiSporocilo").html("");
        $("#preberiEHRid").val($(this).val());
    });

    /**
     * Napolni testne vrednosti (EHR ID, datum in ura, telesna višina,
     * telesna teža, telesna temperatura, sistolični in diastolični krvni tlak,
     * nasičenost krvi s kisikom in merilec) pri vnosu meritve vitalnih znakov
     * bolnika, ko uporabnik izbere vrednosti iz padajočega menuja (npr. Ata Smrk)
     */
    $('#preberiObstojeciVitalniZnak').change(function() {
        $("#dodajMeritveVitalnihZnakovSporocilo").html("");
        var podatki = $(this).val().split("|");
        $("#dodajVitalnoEHR").val(podatki[0]);
        $("#dodajVitalnoDatumInUra").val(podatki[1]);
        $("#dodajVitalnoTelesnaVisina").val(podatki[2]);
        $("#dodajVitalnoTelesnaTeza").val(podatki[3]);
        $("#dodajVitalnoTelesnaTemperatura").val(podatki[4]);
        $("#dodajVitalnoKrvniTlakSistolicni").val(podatki[5]);
        $("#dodajVitalnoKrvniTlakDiastolicni").val(podatki[6]);
    });

    /**
     * Napolni testni EHR ID pri pregledu meritev vitalnih znakov obstoječega
     * bolnika, ko uporabnik izbere vrednost iz padajočega menuja
     * (npr. Ata Smrk, Pujsa Pepa)
     */
    $('#preberiEhrIdZaVitalneZnake').change(function() {
        $("#preberiMeritveVitalnihZnakovSporocilo").html("");
        $("#rezultatMeritveVitalnihZnakov").html("");
        $("#meritveVitalnihZnakovEHRid").val($(this).val());
    });

});